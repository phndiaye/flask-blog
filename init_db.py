# -*- coding: utf-8 -*-
from app import db
from app.models import Users, Posts, Category


# Drop puis recrée toutes les tables de la base
db.drop_all()
db.create_all()

# Crée deux utilisateurs 'admin' et 'guest' puis set leurs firstname/lastname
# (username, email, password)
admin = Users('admin', 'admin@kikoo.lol', 'admin')
guest = Users('guest', 'guest@kikoo.lol', 'guest')

admin.firstname = "Vicky"
admin.lastname = "Mendoza"
guest.firstname = "Lorenzo"
guest.lastname = "Von Matterhorn"


# Crée deux catégories
ipsum = Category('ipsum')
hello = Category('Hello World')

# Crée 3 articles, liés aux catégories
article1 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et \
        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla \
        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est \
        laborum."

article2 = "Upon hearing the frantic project of their leader, each in his own separate soul had suddenly lighted, \
        it would seem, upon the same piece of treachery, namely: to be foremost in breaking out, in order to be the \
        first of the three, though the last of the ten, to surrender; and thereby secure whatever small chance of \
        pardon such conduct might merit."

article3 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et \
        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea \
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla \
        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est \
        laborum."

first_post = Posts('lorem ipsum', article1, 'lorem', ipsum)
first_post.author_id = 1
second_post = Posts('Hello World', article2, 'hello world', hello)
second_post.author_id = 1
third_post = Posts('Second Lorem', article3, 'hello, lorem, second, test', ipsum)
third_post.author_id = 2

# Ajout des utilisateurs
db.session.add(admin)
db.session.add(guest)

# Ajout des catégories
db.session.add(ipsum)
db.session.add(hello)

# Ajout des articles
db.session.add(first_post)
db.session.add(second_post)
db.session.add(third_post)

# Commit pour prendre en compte les changements
db.session.commit()
